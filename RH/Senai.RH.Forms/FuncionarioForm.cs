﻿using RH.Senai.RH._Model;
using RH.Senai.RH.Dao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Forms
{
    public partial class FuncionarioForm : Form
    {
        public FuncionarioForm()
        {
            InitializeComponent();
        }

        private void btnSalvarFuncionario_Click(object sender, EventArgs e)
        {
            // validação
            // verifica se os campos obrigatorios foram
            // preenchidas 
            if (string.IsNullOrEmpty(txtNomeFuncionario.Text)
                && string.IsNullOrEmpty(txtCpfFuncionario.Text)
                && string.IsNullOrEmpty(txtRgFuncionario.Text)
                && string.IsNullOrEmpty(txtEmailFuncionario.Text)
                && string.IsNullOrEmpty(txtTelefoneFuncionario.Text))
            {
                MessageBox.Show("Preencha todos os campos ! ");

            }

            else
            {

                // instancia um funcionário
                Funcionario funcionario = new Funcionario();

                //  obtem o id do funcinario
                // se o Textbox de oid de funcionario Ñ estiver vazio
                // se foi preenchido o TextBox de IDFuncionario
                if (!string.IsNullOrEmpty(txtIDFuncionario.Text))
                {
                    // cria um id
                    long id = 0;
                    // convertre o texto do TextBox para long
                    // armazena na variavel id

                    if (long.TryParse(txtIDFuncionario.Text, out id))
                    {
                        //  atribui o id ao objeto funcionario
                        funcionario.ID = id;
                    }

                }

                // atribui dados ao funcionário
                funcionario.Nome = txtNomeFuncionario.Text;
                funcionario.CPF = txtCpfFuncionario.Text;
                funcionario.RG = txtRgFuncionario.Text;
                funcionario.Email = txtEmailFuncionario.Text;
                funcionario.Telefone = txtTelefoneFuncionario.Text;

                // instancia o dao de funcionários
                FuncionarioDao dao = new FuncionarioDao();

                // salva o funcionário no banco de dados
                dao.Salvar(funcionario);


                // atualiza o data drid view

                PreencheDados();

                LimparFormulario();
            }

        } // fim do evento de click

        // métodos do programador
        private void LimparFormulario()
        {
            txtIDFuncionario.Clear();
            txtNomeFuncionario.Clear();
            txtCpfFuncionario.Clear();
            txtRgFuncionario.Clear();
            txtEmailFuncionario.Clear();
            txtTelefoneFuncionario.Clear();
            txtCpfFuncionario.Focus();
        }

        private void FuncionarioForm_Load(object sender, EventArgs e)
        {
            PreencheDados();
        }

        private void PreencheDados()
        {
            // instancia uma dao
            FuncionarioDao dao = new FuncionarioDao();

            //preenche o data grid view
            dgvFuncionarios.DataSource = dao.Consultar();

            // oculta algumas colunas
            dgvFuncionarios.Columns["ID"].Visible = false; // Ñ APARECE
            dgvFuncionarios.Columns["RG"].Visible = false; // Ñ APARECE

            // limpa a selecção do data grid view
            dgvFuncionarios.ClearSelection();

            // limpa os campos do furmulario
            LimparFormulario();

        }

        private void dgvFuncionarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvFuncionarios_SelectionChanged(object sender, EventArgs e)
        {
            // se alguma linha for selecionada
            if (dgvFuncionarios.CurrentRow != null)
            {
                //pega o id e coloca no textbox de id
                txtIDFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[0].Value.ToString();
                txtNomeFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[1].Value.ToString();
                txtCpfFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[2].Value.ToString();
                txtRgFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[3].Value.ToString();
                txtEmailFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[4].Value.ToString();
                txtTelefoneFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[5].Value.ToString();

            }
        }

        private void btnExcluirFuncionario_Click(object sender, EventArgs e)
        {
            // verifica se o TextBox de Id Funcionario é nulo ou
            // vazio
            // se sim isso significa que nenhum funcionario foi
            // selecionado na lista
            if (string.IsNullOrEmpty(txtIDFuncionario.Text))
            {
                string msg = "Selecione um funcionario na lista abaixo !";
                string titulo = "Operação não realizada ....";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                // instancia um funcionario
                Funcionario funcionario = new Funcionario();

                // cria um id para o funcionario
                long id = 0;

                // converte a string do text box de id para long
                if (long.TryParse(txtIDFuncionario.Text, out id))
                {
                    // atribui o id ao id do funcionario
                    funcionario.ID = id;
                }

                // instacia uma dao
                FuncionarioDao dao = new FuncionarioDao();
                // resposta do usuario
                DialogResult resposta = MessageBox.Show("Deseja realmente excluir este funcionario ?", "Mensagem ...", MessageBoxButtons.YesNo,MessageBoxIcon.Question);
                if (resposta.Equals(DialogResult.Yes))
                {
                    // excui o funcionario
                    dao.Excluir(funcionario);

                    // atualiza p data grid view
                    PreencheDados();
                }
                
            }
            
        }

        private void txtCpfFuncionario_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            //pega o texto do textbox de cpf
            string cpf = txtCpfFuncionario.Text;

            // cria um funcionario

            Funcionario funcionario = new FuncionarioDao().Consultar(cpf);

            // verifica Se Funcionarionão nulo
            if (funcionario != null)
            {
                // preenche os campos do form
                txtIDFuncionario.Text = funcionario.ID.ToString();
                txtCpfFuncionario.Text = funcionario.CPF;
                txtRgFuncionario.Text = funcionario.RG;
                txtNomeFuncionario.Text = funcionario.Nome;
                txtEmailFuncionario.Text = funcionario.Email;
                txtTelefoneFuncionario.Text = funcionario.Telefone;
                
            }


        }
    } // fim da classe
}
