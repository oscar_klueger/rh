﻿using RH.Senai.RH._Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Dao
{
    class FuncionarioDao : IDao<Funcionario>
    {
        // atributos
        // conexão com o banco de dados
        private SqlConnection connection;

        // instrução sql
        private string sql = null;

        // mensagem do messagebox
        private string msg = null;

        // título do messagebox
        private string titulo = null;

        // construtor
        public FuncionarioDao()
        {
            // cria uma conexão com o banco de dados
            connection = new ConnectionFactory().GetConnection();
        }

        // métodos
        public List<Funcionario> Consultar()
        {
            // comando sql de consulta
            sql = "SELECT * FROM Funcionario";

            // lista de funcionários cadastrados
            List<Funcionario> funcionarios = new List<Funcionario>();

            try
            {
                // abre a conexão com o banco de dados
                connection.Open();

                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                // cria um leitor de dados
                SqlDataReader leitor = cmd.ExecuteReader();

                // enquanto o leitor tiver dados para ler
                while (leitor.Read())
                {
                    Funcionario funcionario = new Funcionario();
                    funcionario.ID = (long)leitor["IDFuncionario"];
                    funcionario.Nome = leitor["Nome"].ToString();
                    funcionario.CPF = leitor["Cpf"].ToString();
                    funcionario.RG = leitor["Rg"].ToString();
                    funcionario.Email = leitor["Email"].ToString();
                    funcionario.Telefone = leitor["Telefone"].ToString();
                    // adiciona o funcionário na lista de funcionários
                    funcionarios.Add(funcionario);
                } // fim do while
            }
            catch (SqlException ex)
            {
                msg = "Erro ao consultar os funcionários cadastrados: " + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
            return funcionarios;
        }

        public Funcionario Consultar(string parametro)
        {
            // instruçao sql
            sql = "SELECT * From Funcionario WHERE Cpf = @Cpf";
            Funcionario funcionario = null;

            try
            {
                // abrea a conexão com o banco de dados
                connection.Open();

                //comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@Cpf", parametro);
                //parametro doo comando sql
                ///recebe os dados do banco de dados
                SqlDataReader leitor = cmd.ExecuteReader();

                while (leitor.Read())
                {
                    // compara se o cpf dos registros é igural
                    // ao cpfde parametro
                    if (parametro.Equals(leitor["Cpf"].ToString()))
                    {
                        funcionario = new Funcionario();
                        funcionario.ID = (long)leitor["IDFuncionario"];
                        funcionario.Nome = leitor["Nome"].ToString();
                        funcionario.CPF = leitor["Cpf"].ToString();
                        funcionario.RG = leitor["Rg"]. ToString();
                        funcionario.Email = leitor["Email"].ToString();
                        funcionario.Telefone = leitor["Telefone"].ToString();
                    }// fim do if

                }// fim di while
            }
            catch (SqlException ex)
            {
                msg = "Erro ao comnsulta o funcionario !\n"+ex.Message;
                titulo = "erro ....";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                connection.Close();

            }
            return funcionario;
        }

       

        public void Excluir(Funcionario funcionario)
        {
            // instrução sql
            sql = "DELETE FROM Funcionario WHERE IDFuncionario = @IDFuncionario";
            try
            {
                // abre a conexao com o banco de dados
                connection.Open();

                // cria um comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);
                //adiciona valor ao parametro @IDFuncionario

                cmd.Parameters.AddWithValue("@IDFuncionario", funcionario.ID);

                // executa o comando sql no banco de dados
                cmd.ExecuteNonQuery();


                //mensagem de feedback
                msg = "Funcionario " + funcionario.Nome + " excluido com sucesso";

                // titulo da msg
                titulo = "Sucesso....";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (SqlException ex)
            {
                // mensagem de feedback
                msg = "erro ao excluir o funcionario" + ex.Message;


                //titulo da mensagem
                titulo = "erro ...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }



            finally
            {
                //fecha a conexao co o banco de dadsos
                connection.Close();
            }
        }

        public void Salvar(Funcionario funcionario)
        {         //   verifica se o id funcionario é diferente de 0
            if (funcionario.ID != 0)
            {
                // update
                sql = "UPDATE Funcionario SET Nome=@Nome,Cpf=@Cpf, Rg=@Rg, Email=@Email, Telefone=@Telefone WHERe IDFuncionario = @IDFuncionario";
            }
            else
            {
                // inserte
                sql = "INSERT INTO Funcionario(Nome, Cpf, Rg, Email, Telefone) VALUES (@Nome, @Cpf, @Rg, @Email, @Telefone)";
            }
            try
            {


                // abre uma conexão com o banco de dados
                connection.Open();

                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@IDFuncionario", funcionario.ID);
                cmd.Parameters.AddWithValue("@Nome", funcionario.Nome);
                cmd.Parameters.AddWithValue("@Cpf", funcionario.CPF);
                cmd.Parameters.AddWithValue("@Rg", funcionario.RG);
                cmd.Parameters.AddWithValue("@Email", funcionario.Email);
                cmd.Parameters.AddWithValue("@Telefone", funcionario.Telefone);

                // executa o INSERT
                cmd.ExecuteNonQuery();

                msg = "Funcionario " + funcionario.Nome + " salvo com sucesso !";
                titulo = "Sucesso...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (SqlException ex)
            {
                msg = "Erro o salvar o funcionário: " + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
